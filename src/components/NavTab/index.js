import { Paper, Tabs, Tab } from "@material-ui/core";
import { useState } from "react";
import { useHistory } from "react-router";
import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles({
  paper: {
    position: "fixed",
    width: "100%",
    zIndex: 2,
    backgroundColor: "var(--brown)",
    color: "var(--white)",
    top: 0,
    display: "flex",
    justifyContent: "center",
  },
});

const NavTab = () => {
  const classes = useStyle();
  const [selectedTab, setSelectedTab] = useState(
    Number(localStorage.getItem("@drinksEvents:currentTab")) || 0
  );

  const history = useHistory();

  const handleTabs = (_, newValue) => {
    setSelectedTab(newValue);
    localStorage.setItem("@drinksEvents:currentTab", newValue);
  };

  return (
    <Paper className={classes.paper}>
      <Tabs
        value={selectedTab}
        onChange={handleTabs}
        scrollButtons="on"
        variant="scrollable"
      >
        <Tab label="Catalogue" onClick={() => history.push("/")} />
        <Tab label="Graduation" onClick={() => history.push("/graduation")} />
        <Tab
          label="Get-together"
          onClick={() => history.push("/getTogether")}
        />
        <Tab label="Wedding" onClick={() => history.push("/wedding")} />
      </Tabs>
    </Paper>
  );
};

export default NavTab;
