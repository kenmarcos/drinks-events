import styled from "styled-components";

export const Container = styled.li`
  background-color: var(--black);
  width: 300px;
  display: flex;
  flex-direction: column;
  border: 10px solid var(--brown);
  border-radius: 5px;
  color: var(--white);
  padding: 20px;
  cursor: pointer;
  &:hover {
    filter: brightness(1.2);
    width: 297px;
    box-shadow: 0 0 15px var(--white);
    transition: width 0.3s, box-shadow 0.7s;
  }

  figure {
    background-color: var(--white);
    display: flex;
    justify-content: center;
    position: relative;
    max-height: 250px;
    padding-top: 90%;
    text-align: center;
    border-radius: 5px;
    border: 6px solid var(--brown);
    @media (max-width: 490px) {
      padding-top: 60%;
    }
    img {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      max-height: 90%;
      margin: 5% auto;
      transition: opacity 150ms ease-in-out;
    }
  }

  h2 {
    padding: 10px 0;
  }

  strong {
    padding: 10px 0;
    text-indent: 20px;
    text-align: justify;
    line-height: 23px;
    border-bottom: 1px solid var(--white);
  }

  div {
    padding: 5px 0;
  }
`;
