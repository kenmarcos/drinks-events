import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import CatalogueCard from "./index";

const mockedDrink = {
  name: "Drink Foo",
  image_url: "https://drink-foo.com",
  description: "Description Foo",
  first_brewed: "Test",
  volume: {
    value: 10,
    unit: "litres",
  },
};

const mockedHandleClick = jest.fn();

describe("When everything is ok", () => {
  test("Should appear drink data when the prop drink is passed", () => {
    render(<CatalogueCard drink={mockedDrink} />);

    const nameDrink = screen.queryByText(mockedDrink.name);
    const descriptionDrink = screen.queryByText(mockedDrink.description);
    const firstBrewedDrink = screen.queryByText(/Test/);
    const volumeDrink = screen.queryByText("10 litres");

    expect(nameDrink).toBeInTheDocument();
    expect(descriptionDrink).toBeInTheDocument();
    expect(firstBrewedDrink).toBeInTheDocument();
    expect(volumeDrink).toBeInTheDocument();
  });

  test("Should call handleClick when user clicks on the card", () => {
    render(
      <CatalogueCard drink={mockedDrink} handleClick={mockedHandleClick} />
    );

    const drinkCard = screen.getByRole("listitem");
    userEvent.click(drinkCard);

    expect(mockedHandleClick).toHaveBeenCalled();
  });
});
