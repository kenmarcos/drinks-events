import { createContext, useState, useContext, useEffect } from "react";

const GetTogetherDrinksContext = createContext();

export const GetTogetherDrinksProvider = ({ children }) => {
  const [getTogetherDrinks, setGetTogetherDrinks] = useState([]);

  useEffect(() => {
    setGetTogetherDrinks(
      JSON.parse(localStorage.getItem("@drinksEvents:getTogetherDrinks")) || []
    );
  }, []);

  const addToGetTogetherDrinks = (drink) => {
    const getTogetherDrinksList =
      JSON.parse(localStorage.getItem("@drinksEvents:getTogetherDrinks")) || [];

    const isRepeated = getTogetherDrinks.some((item) => item.id === drink.id);

    if (!isRepeated) {
      getTogetherDrinksList.push(drink);
      localStorage.setItem(
        "@drinksEvents:getTogetherDrinks",
        JSON.stringify(getTogetherDrinksList)
      );

      setGetTogetherDrinks([...getTogetherDrinks, drink]);
    }
  };

  const removeFromGetTogetherDrinks = (index) => {
    const getTogetherDrinksList = getTogetherDrinks.filter(
      (_, idx) => idx !== index
    );

    localStorage.setItem(
      "@drinksEvents:getTogetherDrinks",
      JSON.stringify(getTogetherDrinksList)
    );

    setGetTogetherDrinks(getTogetherDrinksList);
  };

  return (
    <GetTogetherDrinksContext.Provider
      value={{
        getTogetherDrinks,
        addToGetTogetherDrinks,
        removeFromGetTogetherDrinks,
      }}
    >
      {children}
    </GetTogetherDrinksContext.Provider>
  );
};

export const useGetTogetherDrinks = () => useContext(GetTogetherDrinksContext);
