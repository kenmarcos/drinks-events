import axios from "axios";
import { createContext, useState, useContext, useEffect } from "react";

const CatalogueContext = createContext();

export const CatalogueProvider = ({ children }) => {
  const [catalogue, setCatalogue] = useState([]);
  const [page, setPage] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const getDrinks = () => {
    setIsLoading(true);
    axios
      .get(`https://api.punkapi.com/v2/beers?page=${page}&per_page=10`)
      .then((response) => {
        setCatalogue([...catalogue, ...response.data]);
        setIsLoading(false);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    if (page !== 0) {
      getDrinks();
    }
  }, [page]);

  const nextPage = () => {
    setPage((pageInsideState) => pageInsideState + 1);
  };

  // const previousPage = () => {
  //   setPage(page - 1);
  // };

  return (
    <CatalogueContext.Provider value={{ catalogue, page, nextPage, isLoading }}>
      {children}
    </CatalogueContext.Provider>
  );
};

export const useCatalogue = () => useContext(CatalogueContext);
